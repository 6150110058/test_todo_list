import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_todo_list/providers/data_task.dart';
import 'package:test_todo_list/todo_list.dart';

void main() {
  runApp(ChangeNotifierProvider(create: (_) => DataTask(), child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test ToDo List',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const ToDoList(title: 'My Task'),
    );
  }
}
