import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';
import 'package:test_todo_list/providers/data_task.dart';

class ToDoList extends StatefulWidget {
  const ToDoList({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ToDoList> createState() => _ToDoListState();
}

class _ToDoListState extends State<ToDoList> {
  final fromKey = GlobalKey<FormState>();

  final _task = TextEditingController();
  final _date = TextEditingController();

  String? day = '';
  String? month = '';
  String? year = '';

  var maskFormatDate =
      new MaskTextInputFormatter(mask: '##/##/####', filter: {"#": RegExp(r'[0-9]')});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, fontFamily: "Prompt")),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [buildData()],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showAleartDialog();
        },
        tooltip: 'Add Task',
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget buildData() {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: Provider.of<DataTask>(context).num,
        itemBuilder: (context, index) {
          return myCard(Provider.of<DataTask>(context).tasklist[index],
              Provider.of<DataTask>(context).datelist[index]);
        });
  }

  Widget myCard(String task, String date) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Container(
        child: ListTile(
          title: Text("$task", style: TextStyle(fontWeight: FontWeight.w500, fontFamily: "Prompt")),
          subtitle:
              Text("$date", style: TextStyle(fontWeight: FontWeight.w400, fontFamily: "Prompt")),
          onTap: () {},
        ),
      ),
    );
  }

  void showAleartDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        title: Row(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Add Task',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, fontFamily: "Prompt")),
            SizedBox(width: 5),
            Icon(Icons.date_range_outlined)
          ],
        ),
        content: Form(
          key: fromKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFormField(
                controller: _task,
                autofocus: true,
                decoration: InputDecoration(
                    labelText: 'Task',
                    labelStyle:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.w500, fontFamily: "Prompt")),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) {
                  if (_task.text.isEmpty) {
                    return 'กรุณาระบุ';
                  } else {
                    return null;
                  }
                },
              ),
              TextFormField(
                controller: _date,
                autofocus: true,
                // keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: 'Date',
                    labelStyle:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.w500, fontFamily: "Prompt")),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                inputFormatters: [maskFormatDate],
                onChanged: (value) {
                  if (value.length == 10) {
                    day = value.toString().substring(0, 2);
                    month = value.toString().substring(3, 5);
                    year = value.toString().substring(6, 10);
                    if (int.parse(year!) > 3000 || int.parse(month!) > 12 || int.parse(day!) > 31) {
                      day = '';
                      month = '';
                      year = '';
                      _date.text = '';
                    } else {
                      _date.text = day! + '/' + month! + '/' + year!;
                    }
                  }
                },
                validator: (value) {
                  if (_date.text.isEmpty) {
                    return 'กรุณาระบุ';
                  } else {
                    return null;
                  }
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        if (_task.text.isNotEmpty && _date.text.isNotEmpty) {
                          Provider.of<DataTask>(context, listen: false)
                              .inputTask(_task.text, _date.text);
                          Navigator.pop(context);
                          _task.text = '';
                          _date.text = '';
                        } else {
                          fromKey.currentState!.validate();
                        }
                      },
                      child: Text('Add Task',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w500, fontFamily: "Prompt")),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
